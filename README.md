To publish win EXE file please build the solution and run following in Package Manager Console:

    dotnet publish -c Release -r win10-x64
    

update - 27.8.2018    

Notes:

    I've used the TPL library for multithreading. I am amazed how fast the compression is now. 
    
    
Architecture:

    I've introduced a simple default Microsoft DI container for easier extension of the tool. 
    
    
Conversion:

    Is done via Transform/Action/Buffer blocks from TPL library.
    
    
Extensions:
    
    If this tool would be to extended or shared, these would be my following extensions:
        - configuration (CI supported release management, IConfigurationProvider into DI,...)
        - unit tests
        - enhanced exception handling
        
        
Thank you for your time and feedback.

    

    
    
