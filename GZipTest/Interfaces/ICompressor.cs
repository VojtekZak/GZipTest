﻿using System.Threading;
using System.Threading.Tasks;

namespace GZipTest.Interfaces
{
    public interface ICompressor
    {
        Task CompressFileAsync(string inFile, string outFile, CancellationToken cancellationToken);

        Task DecompressFileAsync(string inFile, string outFile, CancellationToken cancellationToken);
    }
}
