﻿using GZipTest.Interfaces;
using System;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace GZipTest
{
    public class GzipCompressor : ICompressor
    {
        private readonly int bufferSize;
        private readonly int boundedCapacity;

        public GzipCompressor(int bufferCopySize, int bufferBoundedCapacity)
        {
            bufferSize = bufferCopySize;
            boundedCapacity = bufferBoundedCapacity;
        }

        public GzipCompressor()
        {
            // default bufffer size from GZipStream
            bufferSize = 81920;

            // TODO: should be configurable (outside the code)
            boundedCapacity = 100;
        }

        private byte[] ProcessData(byte[] data, CompressionMode compressionMode)
        {
            using (var sourceStream = new MemoryStream(data))
            {
                using (var resultStream = new MemoryStream())
                {
                    switch (compressionMode)
                    {
                        case CompressionMode.Compress:
                            using (var zipStream = new GZipStream(resultStream, CompressionMode.Compress))
                            {
                                sourceStream.CopyTo(zipStream);
                            }
                            break;
                        case CompressionMode.Decompress:
                            using (var zipStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                            {
                                zipStream.CopyTo(resultStream);
                            }
                            break;
                    }
                    return resultStream.ToArray();
                }
            }
        }

        public async Task CompressFileAsync(string inFile, string outFile, CancellationToken cancellationToken)
        {
            using (var sourceFileStream = File.OpenRead(inFile))
            {
                using (var outFileStream = File.Create(outFile))
                {
                    await ProcessFileMultiThread(sourceFileStream, outFileStream, CompressionMode.Compress, cancellationToken);
                }
            }
        }

        public async Task DecompressFileAsync(string inFile, string outFile, CancellationToken cancellationToken)
        {
            using (var sourceFileStream = File.OpenRead(inFile))
            {
                using (var outFileStream = File.Create(outFile))
                {
                    await ProcessFileMultiThread(sourceFileStream, outFileStream, CompressionMode.Decompress, cancellationToken);
                }
            }
        }


        public Task ProcessFileMultiThread(Stream inputStream, Stream outputStream, CompressionMode compressionMode, CancellationToken cancellationToken)
        {

            var compressor = new TransformBlock<byte[], byte[]>(
                    data => ProcessData(data, compressionMode),
                    new ExecutionDataflowBlockOptions
                    {
                        MaxDegreeOfParallelism = ExecutionDataflowBlockOptions.Unbounded,
                        BoundedCapacity = boundedCapacity
                    });

            var writer = new ActionBlock<byte[]>(
                data => outputStream.Write(data, 0, data.Length),
                new ExecutionDataflowBlockOptions
                {
                    BoundedCapacity = boundedCapacity,
                    SingleProducerConstrained = true
                });

            var buffer = new BufferBlock<byte[]>(
                new DataflowBlockOptions
                {
                    BoundedCapacity = boundedCapacity
                });

            buffer.LinkTo(compressor);
            buffer.Completion.ContinueWith(task => compressor.Complete());

            compressor.LinkTo(writer);
            compressor.Completion.ContinueWith(task => writer.Complete());

            var readBuffer = new byte[bufferSize];
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    buffer.Complete();
                    break;
                }

                int readCount = inputStream.Read(readBuffer, 0, bufferSize);
                if (readCount > 0)
                {
                    var postData = new byte[readCount];
                    Buffer.BlockCopy(readBuffer, 0, postData, 0, readCount);
                    while (!buffer.Post(postData))
                    {
                    }
                }
                if (readCount != bufferSize)
                {
                    buffer.Complete();
                    break;
                }
            }

            return writer.Completion;
        }
    }
}

