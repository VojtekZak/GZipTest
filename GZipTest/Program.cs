﻿using GZipTest.Interfaces;
using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using Microsoft.Extensions.Logging;


namespace GZipTest
{
    class Program
    {
        private const string compress = "compress";
        private const string decompress = "decompress";

        private static CancellationTokenSource cts = new CancellationTokenSource();

        static int Main(string[] args)
        {
            // Setup DI
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddTransient<ICompressor, GzipCompressor>()
                .BuildServiceProvider();

            // log to console
            serviceProvider
                .GetService<ILoggerFactory>()
                .AddConsole();

            // logger
            var logger = serviceProvider.GetService<ILoggerFactory>().CreateLogger<Program>();

            // register cancellation 
            Console.CancelKeyPress += new ConsoleCancelEventHandler(cancelHandler);

            // validation
            try
            {
                ValidateInput(args);

                // logic
                logger.LogInformation($"Process started");
                try
                {
                    switch (args[0])
                    {
                        case compress:
                            serviceProvider.GetService<ICompressor>().CompressFileAsync(args[1], args[2], cts.Token).Wait(cts.Token);
                            break;

                        case decompress:
                            serviceProvider.GetService<ICompressor>().DecompressFileAsync(args[1], args[2], cts.Token).Wait(cts.Token);
                            break;
                    }
                }
                catch (Exception e)
                {
                    logger.LogError(e.ToString());
                    
                    // cleanup
                    if (File.Exists(args[2]))
                    {
                        File.Delete(args[2]);
                    }

                    return 1;
                }

            }
            catch (Exception e)
            {
                logger.LogWarning(e.Message);
                return 1;
            }

            // success
            logger.LogInformation("Process finished successfuly");
            return 0;
        }

        private static void cancelHandler(object sender, ConsoleCancelEventArgs e)
        {
            // prevent termination of app
            e.Cancel = true;

            // cancel running processes
            cts.Cancel();
        }

        private static void ValidateInput(string[] args)
        {
            if (args.Length != 3)
            {
                throw new Exception(GetHelpMessage());
            }

            if (!((args[0].CompareTo(compress) == 0) || (args[0].CompareTo(decompress) == 0)))
            {
                throw new Exception(GetHelpMessage());
            }

            if (!(File.Exists(args[1])))
            {
                throw new Exception($"The file \"{args[1]}\" could not be found.");
            }
        }

        private static string GetHelpMessage()
        {
            StringBuilder messageBuilder = new StringBuilder();
            messageBuilder.AppendLine("Please use following syntax:");
            messageBuilder.AppendLine("     compressing: GZipTest.exe compress [original file name] [archive file name]");
            messageBuilder.AppendLine("     decompressing: GZipTest.exe decompress [archive file name] [decompressing file name]");

            return messageBuilder.ToString();
        }
    }
}
